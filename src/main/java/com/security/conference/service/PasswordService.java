package com.security.conference.service;

import com.security.conference.model.Password;
import com.security.conference.model.ResetToken;

public interface PasswordService {

    void createResetToken(Password password, String token);

    boolean confirmResetToken(ResetToken token);

    void update(Password password, String username);

}
