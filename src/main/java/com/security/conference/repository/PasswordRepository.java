package com.security.conference.repository;

import com.security.conference.model.Password;
import com.security.conference.model.ResetToken;

public interface PasswordRepository {
    void saveToken(ResetToken resetToken);

    ResetToken findByToken(String token);

    void update(Password password, String username);
}
