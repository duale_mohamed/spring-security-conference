package com.security.conference.repository;

import com.security.conference.model.Account;
import com.security.conference.model.ConferenceUserDetails;
import com.security.conference.model.VerificationToken;

public interface AccountRepository {
    public Account create (Account account);

    void saveToken(VerificationToken verificationToken);

    VerificationToken findByToken(String token);

    Account findByUsername(String username);

    void createUserDetails(ConferenceUserDetails userDetails);

    void createAuthorities(ConferenceUserDetails userDetails);

    void delete(Account account);

    void deleteToken(String token);
}
